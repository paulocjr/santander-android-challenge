## Android Developer - PAGOFX - Banco Santander - Android Challenge

This project is a challenge for an Android position at PAGOFX - Banco Santander.

## About

The application uses an external API (https://dev-server-demos.herokuapp.com/) that retrieves all user products such as Accounts and Cards.

## Project details

#### API
The API demo. (Docs): [https://dev-server-demos.herokuapp.com/]

#### Architecture and Libraries

* Architecture MVVM ✔
* Kotlin Language <3 ✔
* API minimum 21 and target 29 ✔
* Client REST/HTTP with Retrofit + OkHttp ✔
* View binding using Data binding ✔
* Dependency injection - Dagger2 ✔
* Unit Tests with JUnit + Mockito ✔
* UI Tests with Expresso ✔

#### Additional

* Catch error cases: connection network and server errors ✔︎
* Parallax effect on Appbar ✔︎


## Licença

    The MIT License (MIT)

    Copyright (c) 2019 Paulo Camilo

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.