package com.santander.challenge.viewmodel.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.santander.challenge.models.BaseData
import com.santander.challenge.models.product.ProductResult
import com.santander.challenge.models.product.ProductType
import com.santander.challenge.models.product.UserProduct
import com.santander.challenge.repository.user.UserRepository
import com.santander.challenge.service.models.AuthToken
import com.santander.challenge.service.models.UserProducts
import com.santander.challenge.viewmodel.base.BaseViewModel

/**
 * Created by paulo on 19-10-2019.
 */
class UserViewModel(userRepository: UserRepository)  : BaseViewModel() {

    private var mUseRepository: UserRepository = userRepository

    /**
     * Request a login
     */
    fun login(user: String, password: String): LiveData<BaseData<AuthToken>> {
        showLoading()
        return Transformations.map(mUseRepository.login(user = user, password = password)) { data ->
            data.let {

                val mData = MutableLiveData<BaseData<AuthToken>>()

                if (it.success) {
                    mData.value = BaseData(true, it.data)
                } else {
                    mData.value = BaseData(false, null)
                }

                hideLoading()
                mData.value


            }
        }
    }

    /**
     * Get the user products
     */
    fun userProducts(): LiveData<BaseData<UserProduct>> {
        showLoading()
        return Transformations.map(mUseRepository.getUserProducts()) { data ->
            data.let {

                val mData = MutableLiveData<BaseData<UserProduct>>()

                if (it.success && it.data != null) {
                    mData.value = BaseData(true, UserProduct(it.data.name, convertToProductList(it.data.accounts, it.data.cards)))
                } else {
                    mData.value = BaseData(false, null)
                }

                hideLoading()
                mData.value
            }
        }
    }

    private fun convertToProductList(accounts: List<UserProducts.Account>, cards: List<UserProducts.Card>): List<ProductResult> {
        val listProducts = arrayListOf<ProductResult>()

        // Added an accounts header
        listProducts.add(createHeader(ProductType.ITEM_ACCOUNT.value))
        // Get all accounts
        accounts.let {
            for (account in accounts) {
                listProducts.add(ProductResult("", account.iban, account.alias, account.balance, ProductType.ITEM_ACCOUNT))
            }
        }

        // Added a cards header
        listProducts.add(createHeader(ProductType.ITEM_CARD.value))
        // Get all cards
        cards.let {
            for (card in cards) {
                listProducts.add(ProductResult("", card.pan, card.alias, card.available, ProductType.ITEM_CARD))
            }
        }

        return listProducts
    }

    private fun createHeader(title: String): ProductResult {
        return ProductResult(title, "", "", null, ProductType.ITEM_HEADER)
    }
}