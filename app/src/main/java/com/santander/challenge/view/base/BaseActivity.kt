package com.santander.challenge.view.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.santander.challenge.SantanderApplication

/**
 * Created by paulo on 19-10-2019.
 */
abstract class BaseActivity<T : ViewDataBinding> (@LayoutRes private val layoutResId: Int) : AppCompatActivity() {

    private lateinit var mDataBinding: T

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDataBinding = DataBindingUtil.setContentView(this, layoutResId)
        initInjectors()
        initBinding()
    }

    /**
     * Returns the current binding of layout
     *
     * @return the T is a generic type
     */
    fun getBinding(): T = mDataBinding

    /**
     * Initialize the injectors for dagger components
     */
    protected abstract fun initInjectors()

    /**
     * Initialize the binding for layouts in Activities or Fragments
     */
    protected abstract fun initBinding()

    /**
     * Returns the App Component from Application
     */
    fun getAppComponent() = SantanderApplication.getInstance().getAppComponent()
}