package com.santander.challenge.view.detail

import android.graphics.PorterDuff
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import com.santander.challenge.R
import com.santander.challenge.databinding.ActivityDetailBinding
import com.santander.challenge.di.module.detail.DaggerDetailComponent
import com.santander.challenge.models.product.ProductResult
import com.santander.challenge.view.base.BaseActivity
import android.content.Intent
import com.santander.challenge.view.info.InfoActivity


class DetailActivity : BaseActivity<ActivityDetailBinding>(R.layout.activity_detail) {

    lateinit var mProductResult: ProductResult
    var positionChange = 0

    override fun initInjectors() {
        DaggerDetailComponent.builder()
            .appComponent(getAppComponent())
            .build().inject(this)
    }

    override fun initBinding() {

        val intentExtras = intent.extras

        if (intentExtras != null) {
            mProductResult = intentExtras.getSerializable(ProductResult.EXTRA_PARAM) as ProductResult
            positionChange = intentExtras.getInt(ProductResult.EXTRA_POSITION)
            getBinding().model = mProductResult
            changeColorValue()
            setupToolbar()
        } else {
            finish()
        }

        listeners()
    }

    private fun listeners() {
        getBinding().edtAlias.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s.let {
                    mProductResult.alias = s.toString()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        getBinding().tvProjectSource.setOnClickListener {
            startActivity(Intent(this, InfoActivity::class.java))
        }
    }

    private fun changeColorValue() {
        getBinding().tvProductValue.text = mProductResult.attribute!!.value
        getBinding().tvProductCurrency.text = mProductResult.attribute!!.currency

        //change color text value
        if(mProductResult.attribute!!.value.startsWith("+")) {
            getBinding().tvProductValue.setTextColor(resources.getColor(R.color.colorCredit))
            getBinding().tvProductCurrency.setTextColor(resources.getColor(R.color.colorCredit))
        } else {
            getBinding().tvProductValue.setTextColor(resources.getColor(R.color.colorDebit))
            getBinding().tvProductCurrency.setTextColor(resources.getColor(R.color.colorDebit))
        }
    }

    private fun setupToolbar() {
        getBinding().tbProductDetail.title = getString(R.string.product_detail)
        setSupportActionBar(getBinding().tbProductDetail)
        val upArrow = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp).mutate()
        upArrow.setColorFilter(resources.getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Respond to the action bar's Up/Home button
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val data = Intent()
        data.putExtra(ProductResult.EXTRA_PARAM, mProductResult)
        data.putExtra(ProductResult.EXTRA_POSITION, positionChange)
        setResult(ProductResult.RESULT_OK, data)
        super.onBackPressed()
    }

}
