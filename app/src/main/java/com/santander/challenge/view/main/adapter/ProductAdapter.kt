package com.santander.challenge.view.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.santander.challenge.R
import com.santander.challenge.databinding.ItemListHeaderBinding
import com.santander.challenge.databinding.LayoutItemProductsBinding
import com.santander.challenge.models.product.ProductResult
import com.santander.challenge.models.product.ProductType

/**
 * Created by paulo on 20-10-2019.
 */
class ProductAdapter(listener: ProductAdapterListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mListData = arrayListOf<ProductResult>()
    private var mListener: ProductAdapterListener = listener

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(viewGroup.context)

        when(ProductType.values()[viewType]) {

            ProductType.ITEM_HEADER -> {
                val binding = ItemListHeaderBinding.inflate(inflater)
                viewHolder = HeaderViewHolder(binding)

            }

            ProductType.ITEM_CARD , ProductType.ITEM_ACCOUNT -> {
                val binding = LayoutItemProductsBinding.inflate(inflater)
                viewHolder = ProductViewHolder(binding)
            }

        }
        return viewHolder
    }

    override fun getItemCount(): Int = mListData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val productItem = mListData[position]

        when (productItem.productResultType) {
            ProductType.ITEM_HEADER -> {
                val headerViewHolder =  holder as HeaderViewHolder
                headerViewHolder.bind(productItem)
            }

            ProductType.ITEM_ACCOUNT , ProductType.ITEM_CARD -> {
                val productViewHolder =  holder as ProductViewHolder
                productViewHolder.bind(productItem)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (mListData[position].productResultType) {
            ProductType.ITEM_HEADER -> ProductType.ITEM_HEADER.itemType
            ProductType.ITEM_ACCOUNT -> ProductType.ITEM_ACCOUNT.itemType
            ProductType.ITEM_CARD -> ProductType.ITEM_CARD.itemType
            else -> ProductType.ITEM_ACCOUNT.itemType
        }
    }

    // view holder for layout = layout_item_product.xml
    inner class ProductViewHolder(private val binding: LayoutItemProductsBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ProductResult) {
            binding.viewModel = item

            item.attribute.let {
                binding.tvProductCurrency.text = item.attribute!!.currency

                //change color text value
                if(item.attribute!!.value.startsWith("+")) {
                    binding.tvProductValue.setTextColor(binding.view.resources.getColor(R.color.colorCredit))
                    binding.tvProductCurrency.setTextColor(binding.view.resources.getColor(R.color.colorCredit))
                } else {
                    binding.tvProductValue.setTextColor(binding.view.resources.getColor(R.color.colorDebit))
                    binding.tvProductCurrency.setTextColor(binding.view.resources.getColor(R.color.colorDebit))
                }

                binding.tvProductValue.text = item.attribute!!.value
            }

            // change icon if account or card attribute
            if (item.productResultType == ProductType.ITEM_ACCOUNT) {
                binding.ivProductIcon.setImageDrawable(ContextCompat.getDrawable(binding.view.context, R.drawable.ic_accounts))
            } else if (item.productResultType == ProductType.ITEM_CARD) {
                binding.ivProductIcon.setImageDrawable(ContextCompat.getDrawable(binding.view.context, R.drawable.ic_cards))
            }

            binding.contentMain.setOnClickListener {
                mListener.onProductResultClicked(item, adapterPosition)
            }

            binding.executePendingBindings()
        }
    }

    // view holder for layout = layout_item_header.xml
    inner class HeaderViewHolder(private val binding: ItemListHeaderBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ProductResult) {
            binding.viewModel = item
            binding.executePendingBindings()
        }
    }

    /**
     * Set all data in layout item
     *
     * @param data the data with elements
     */
    fun submitList(data: List<ProductResult>) {
        this.mListData = data as ArrayList<ProductResult>
        notifyDataSetChanged()
    }


    /**
     * Update specific item on list
     *
     */
    fun updateItem(item: ProductResult, position: Int) {
        this.mListData[position] = item
        notifyItemChanged(position)
    }

    interface ProductAdapterListener {

        /**
         * On ProductResult item clicked
         */
        fun onProductResultClicked(item: ProductResult, itemPosition: Int)
    }

}