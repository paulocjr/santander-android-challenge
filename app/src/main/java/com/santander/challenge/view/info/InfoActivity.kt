package com.santander.challenge.view.info

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_info.*
import android.content.Intent
import android.net.Uri
import com.santander.challenge.R


class InfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        icLogo.setOnClickListener {
            openProject()
        }
    }

    private fun openProject(){
        val browserIntent = Intent(Intent.ACTION_VIEW)
        browserIntent.data = Uri.parse(getString(R.string.source_code_project_url))
        startActivity(browserIntent)
    }
}
