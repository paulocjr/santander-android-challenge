package com.santander.challenge.view.login

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.databinding.Observable
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.santander.challenge.R
import com.santander.challenge.databinding.ActivityLoginBinding
import com.santander.challenge.di.module.login.DaggerLoginComponent
import com.santander.challenge.repository.user.UserRepository
import com.santander.challenge.utils.Utils
import com.santander.challenge.utils.sharedpreferences.UserAccountPreferences
import com.santander.challenge.view.base.BaseActivity
import com.santander.challenge.view.main.MainActivity
import com.santander.challenge.viewmodel.base.getViewModel
import com.santander.challenge.viewmodel.user.UserViewModel
import javax.inject.Inject

class LoginActivity : BaseActivity<ActivityLoginBinding>(R.layout.activity_login) {

    private var mSnackBar: Snackbar? = null

    @Inject
    lateinit var userRepository: UserRepository
    private val mUserViewModel: UserViewModel by lazy {
        getViewModel { UserViewModel(userRepository) }
    }

    override fun initInjectors() {
        DaggerLoginComponent.builder()
            .appComponent(getAppComponent())
            .build().inject(this)
    }

    override fun initBinding() {
        getBinding().apply {
            viewModel = mUserViewModel
            lifecycleOwner = this@LoginActivity
        }

        setupListeners()
        getUserLogged()
    }

    /**
     * Get the user logged
     */
    private fun getUserLogged() {
        val userLogged = UserAccountPreferences.userLogged
        userLogged.let {
            getBinding().edtUser.setText(it)
        }
    }

    private fun setupListeners() {
        mUserViewModel.showLoading.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (mUserViewModel.showLoading.get() == View.VISIBLE) {
                    getBinding().btnLogin.visibility = View.GONE
                } else if (mUserViewModel.showLoading.get() == View.GONE) {
                    getBinding().btnLogin.visibility = View.VISIBLE
                }
            }
        })

        // OnClick login button
        getBinding().btnLogin.setOnClickListener {
            if (Utils.isOnline(this))
                login()
            else {
                showErrorMessage(getString(R.string.warning_no_internet), BaseTransientBottomBar.LENGTH_SHORT)
            }
        }

        // EditText User
        getBinding().edtUser.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                checkRequiredFields()
            }
        })

        // EditText Password
        getBinding().edtPassword.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                checkRequiredFields()
            }
        })
    }

    /**
     * Show an error message with SnackBar
     */
    private fun showErrorMessage(message: String, duration: Int) {
        mSnackBar = Snackbar.make(findViewById(R.id.content_login), message, duration)
        mSnackBar?.show()
    }

    /**
     * Request a login
     */
    private fun login() {
        mUserViewModel.login(getBinding().edtUser.text.toString(),
            getBinding().edtPassword.text.toString()).observe(this, Observer { res ->

            if (res.data != null && res.data?.tokenCredential != null) {
                loginSuccess(res.data?.tokenCredential!!)
            } else {
                showErrorMessage(getString(R.string.warning_credentials_invalid), BaseTransientBottomBar.LENGTH_SHORT)
            }
        })
    }

    /**
     * Login success and go to Main Activity
     */
    private fun loginSuccess(tokenCredential: String) {
        UserAccountPreferences.authToken = tokenCredential
        UserAccountPreferences.userLogged = getBinding().edtUser.text.toString()
        startMainActivity()
    }

    /**
     * Start the main activity
     */
    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    /**
     * Check if fields are validated to enable login button
     */
    fun checkRequiredFields(){
        getBinding().btnLogin.isEnabled = getBinding().edtUser.text!!.isNotEmpty() && getBinding().edtPassword.text!!.isNotEmpty()
    }
}
