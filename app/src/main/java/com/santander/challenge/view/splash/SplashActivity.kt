package com.santander.challenge.view.splash

import android.content.Intent
import android.os.Handler
import com.santander.challenge.R
import com.santander.challenge.databinding.ActivitySplashBinding
import com.santander.challenge.di.module.splash.DaggerSplashComponent
import com.santander.challenge.view.base.BaseActivity
import com.santander.challenge.view.login.LoginActivity

class SplashActivity : BaseActivity<ActivitySplashBinding>(R.layout.activity_splash) {

    override fun initInjectors() {
        DaggerSplashComponent.builder()
            .appComponent(getAppComponent())
            .build().inject(this)
    }

    override fun initBinding() {
        Handler().postDelayed({
            startLoginActivity()
        }, 2000)
    }

    /**
     * Start Login activity
     */
    private fun startLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out)
        finish()
    }

}
