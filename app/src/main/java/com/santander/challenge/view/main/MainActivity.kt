package com.santander.challenge.view.main

import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.santander.challenge.R
import com.santander.challenge.databinding.ActivityMainBinding
import com.santander.challenge.di.module.main.DaggerMainComponent
import com.santander.challenge.models.product.ProductResult
import com.santander.challenge.models.product.UserProduct
import com.santander.challenge.repository.user.UserRepository
import com.santander.challenge.view.base.BaseActivity
import com.santander.challenge.view.detail.DetailActivity
import com.santander.challenge.view.main.adapter.ProductAdapter
import com.santander.challenge.viewmodel.base.getViewModel
import com.santander.challenge.viewmodel.user.UserViewModel
import javax.inject.Inject


class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main) , ProductAdapter.ProductAdapterListener {

    @Inject
    lateinit var mUserRepository: UserRepository

    private val mUserViewModel: UserViewModel by lazy {
        getViewModel { UserViewModel(mUserRepository) }
    }

    private var mProductAdapter: ProductAdapter? = null

    override fun initInjectors() {
        DaggerMainComponent.builder()
            .appComponent(getAppComponent())
            .build().inject(this)
    }

    override fun initBinding() {
        getBinding().apply {
            viewModel = mUserViewModel
            lifecycleOwner = this@MainActivity
        }

        getUserProducts()

    }

    private fun getUserProducts() {
        mUserViewModel.userProducts().observe(this, Observer { res ->

            if (res.data != null && res.success) {
                updateUI(res.data!!)
            } else {
                showErrorMessage()
            }
        })
    }

    override fun onProductResultClicked(item: ProductResult, itemPosition: Int) {
        val intentDetail = Intent(this, DetailActivity::class.java)

        intentDetail.putExtra(ProductResult.EXTRA_PARAM, item)
        intentDetail.putExtra(ProductResult.EXTRA_POSITION, itemPosition)

       startActivityForResult(intentDetail, ProductResult.RESULT_OK)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == ProductResult.RESULT_OK) {
            data?.extras?.let {
                val result = data.extras!!.getSerializable(ProductResult.EXTRA_PARAM) as ProductResult
                val positionToUpdate = data.extras!!.getInt(ProductResult.EXTRA_POSITION)
                updateItemList(result, positionToUpdate)
            }
        }
    }

    /**
     * Update specific item on list of data
     */
    private fun updateItemList(result: ProductResult, positionToUpdate: Int) {
        mProductAdapter?.updateItem(result, positionToUpdate)
    }

    private fun showErrorMessage() {
        getBinding().rvProducts.visibility = View.GONE
        getBinding().contentError.visibility = View.VISIBLE
    }

    private fun updateUI(userProducts: UserProduct) {
        getBinding().tbUserProducts.title = userProducts.userName
        setSupportActionBar(getBinding().tbUserProducts)
        setupToolbar()

        setupProductsList(userProducts.products)
    }

    private fun setupProductsList(products: List<ProductResult>) {
        getBinding().rvProducts.visibility = View.VISIBLE
        getBinding().contentError.visibility = View.GONE

        products.let {
            if (mProductAdapter == null) {
                mProductAdapter = ProductAdapter(this)
                mProductAdapter?.submitList(products)

                getBinding().rvProducts.adapter = mProductAdapter
                val manager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                getBinding().rvProducts.layoutManager = manager
            } else {
                mProductAdapter?.submitList(products)
            }
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(getBinding().tbUserProducts)
        getBinding().collapsingToolbar.setExpandedTitleColor(resources.getColor(R.color.colorWhite))
        getBinding().collapsingToolbar.setCollapsedTitleTextColor(resources.getColor(R.color.colorWhite))
    }

}
