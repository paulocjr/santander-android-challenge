package com.santander.challenge.repository.user

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.santander.challenge.service.APIClient
import com.santander.challenge.service.UserService
import com.santander.challenge.service.models.AuthToken
import com.santander.challenge.service.models.BaseResponse
import com.santander.challenge.service.models.User
import com.santander.challenge.service.models.UserProducts
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by paulo on 19-10-2019.
 */
class UserRepository @Inject constructor (apiClient: APIClient) : IUserRepository {

    private val TAG = UserRepository::class.java.simpleName
    private val mUserService: UserService = apiClient.getRetrofit().create(UserService::class.java)

    override fun login(user: String, password: String): LiveData<BaseResponse<AuthToken>> {
        val data: MutableLiveData<BaseResponse<AuthToken>> = MutableLiveData()

        mUserService.login(User(user, password))
            .enqueue(object : Callback<AuthToken> {

                override fun onResponse(call: Call<AuthToken>, response: Response<AuthToken>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            data.value = BaseResponse(true, response.body())
                        } else {
                            data.value = BaseResponse(false, null)
                        }
                    } else {
                        data.value = BaseResponse(false, null)
                        Log.e(TAG, response.message())
                    }
                }

                override fun onFailure(call: Call<AuthToken>, t: Throwable) {
                    data.value = BaseResponse(false, null)
                    t.message.let {
                        Log.e(TAG, it.toString())
                    }
                }
            })

        return data
    }

    override fun getUserProducts(): LiveData<BaseResponse<UserProducts>> {
        val data: MutableLiveData<BaseResponse<UserProducts>> = MutableLiveData()

        mUserService.getUserProducts()
            .enqueue(object : Callback<UserProducts> {

                override fun onResponse(call: Call<UserProducts>, response: Response<UserProducts>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            data.value = BaseResponse(true, response.body())
                        } else {
                            data.value = BaseResponse(false, null)
                        }
                    } else {
                        data.value = BaseResponse(false, null)
                        Log.e(TAG, response.message())
                    }
                }

                override fun onFailure(call: Call<UserProducts>, t: Throwable) {
                    data.value = BaseResponse(false, null)
                    t.message.let {
                        Log.e(TAG, it.toString())
                    }
                }
            })

        return data
    }
}