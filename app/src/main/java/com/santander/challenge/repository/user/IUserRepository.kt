package com.santander.challenge.repository.user

import androidx.lifecycle.LiveData
import com.santander.challenge.service.models.AuthToken
import com.santander.challenge.service.models.BaseResponse
import com.santander.challenge.service.models.UserProducts

/**
 * Created by paulo on 19-10-2019.
 */
interface IUserRepository {

    /**
     * Request a login in backend service
     */
    fun login(user: String, password: String) : LiveData<BaseResponse<AuthToken>>

    /**
     * Retrieve all user products
     */
    fun getUserProducts() : LiveData<BaseResponse<UserProducts>>

}