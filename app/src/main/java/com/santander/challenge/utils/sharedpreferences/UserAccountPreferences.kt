package com.santander.challenge.utils.sharedpreferences

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by paulo on 19-10-2019.
 */
    object UserAccountPreferences {

    private const val NAME = "user_bank_account"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences
    private val AUTH_TOKEN = Pair("auth_token", "")
    private val USER_LOGGED = Pair("user_logged", "")

    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    var authToken: String
        get() = preferences.getString(AUTH_TOKEN.first, AUTH_TOKEN.second).toString()
        set(value) = preferences.edit {
            it.putString(AUTH_TOKEN.first, value)
        }

    var userLogged: String
        get() = preferences.getString(USER_LOGGED.first, USER_LOGGED.second).toString()
        set(value) = preferences.edit {
            it.putString(USER_LOGGED.first, value)
        }

}