package com.santander.challenge.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * Created by paulo on 19-10-2019.
 */
class Utils {

    companion object {

        /**
         * Returns if the application is connected in the network
         */
        fun isOnline(context: Context?): Boolean {
            return if (context != null) {
                val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val netInfo: NetworkInfo?
                netInfo = cm.activeNetworkInfo
                netInfo != null && netInfo.isConnected
            } else {
                false
            }
        }

    }

}