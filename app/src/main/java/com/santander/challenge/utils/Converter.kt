package com.santander.challenge.utils

import com.google.gson.Gson
import com.santander.challenge.models.product.ProductResult

/**
 * Created by paulo on 20-10-2019.
 */
class Converter {

    companion object {

        fun convertToJson(prd: ProductResult): String {
            return Gson().toJson(prd)
        }

        fun convertToObject(json: String): ProductResult? {
            return Gson().fromJson(json, ProductResult::class.java)
        }
    }
}