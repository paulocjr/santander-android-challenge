package com.santander.challenge.di.module.splash

import com.santander.challenge.di.app.AppComponent
import com.santander.challenge.di.scope.Activity
import com.santander.challenge.view.splash.SplashActivity
import dagger.Component

/**
 * Created by paulo on 19-10-2019.
 */
@Activity
@Component(dependencies = [AppComponent::class])
interface SplashComponent {
    fun inject(activity: SplashActivity)
}