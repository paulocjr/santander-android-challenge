package com.santander.challenge.di.module.login

import com.santander.challenge.di.app.AppComponent
import com.santander.challenge.di.scope.Activity
import com.santander.challenge.view.login.LoginActivity
import dagger.Component

/**
 * Created by paulo on 19-10-2019.
 */
@Activity
@Component(dependencies = [AppComponent::class])
interface LoginComponent {
    fun inject(activity: LoginActivity)
}