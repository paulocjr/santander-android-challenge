package com.santander.challenge.di.scope

import javax.inject.Scope

/**
 * Created by paulo on 19-10-2019.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class Activity