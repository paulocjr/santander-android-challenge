package com.santander.challenge.di.app

import androidx.lifecycle.ViewModelProvider
import com.santander.challenge.viewmodel.base.ViewModelFactory
import dagger.Binds
import dagger.Module

/**
 * Created by paulo on 19-10-2019.
 */
@Module
abstract class ViewModelBuilder {

    // ViewModel Factory
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}
