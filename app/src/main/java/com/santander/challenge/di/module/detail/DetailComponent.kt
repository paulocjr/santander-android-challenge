package com.santander.challenge.di.module.detail

import com.santander.challenge.di.app.AppComponent
import com.santander.challenge.di.scope.Activity
import com.santander.challenge.view.detail.DetailActivity
import dagger.Component

/**
 * Created by paulo on 19-10-2019.
 */
@Activity
@Component(dependencies = [AppComponent::class])
interface DetailComponent {
    fun inject(activity: DetailActivity)
}