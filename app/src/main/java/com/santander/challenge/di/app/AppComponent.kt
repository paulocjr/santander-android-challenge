package com.santander.challenge.di.app

import android.app.Application
import android.content.Context
import com.santander.challenge.repository.user.UserRepository
import com.santander.challenge.service.APIClient
import dagger.Component
import javax.inject.Singleton

/**
 * Created by paulo on 19-10-2019.
 */
@Singleton
@Component(
    modules = [AppModule::class, ViewModelBuilder::class])
interface AppComponent {
    fun application() = Application()
    fun context(): Context
    fun apiClient(): APIClient
    fun userRepository() : UserRepository
}