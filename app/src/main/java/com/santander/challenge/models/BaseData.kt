package com.santander.challenge.models

/**
 * Created by paulo on 19-10-2019.
 */
class BaseData<T>(var success: Boolean, var data: T?)