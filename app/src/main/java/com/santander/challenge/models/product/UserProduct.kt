package com.santander.challenge.models.product

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.santander.challenge.BR

/**
 * Created by paulo on 20-10-2019.
 */
class UserProduct(var _userName: String, var _products: List<ProductResult>) : BaseObservable(){

    var userName: String
        @Bindable get() = _userName
        set(value) {
            _userName = value
            notifyPropertyChanged(BR.userName)
        }

    var products: List<ProductResult>
        @Bindable get() = _products
        set(value) {
            _products = value
            notifyPropertyChanged(BR.products)
        }
}