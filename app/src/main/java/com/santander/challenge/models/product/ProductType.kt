package com.santander.challenge.models.product

import java.io.Serializable

/**
 * Created by paulo on 20-10-2019.
 */
enum class ProductType(val itemType: Int, val value: String) : Serializable {
    ITEM_HEADER(0, "Header"),
    ITEM_CARD(1, "Cards"),
    ITEM_ACCOUNT(2, "Accounts")
}