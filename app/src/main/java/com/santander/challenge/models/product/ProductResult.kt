package com.santander.challenge.models.product

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.santander.challenge.BR
import com.santander.challenge.service.models.UserProducts
import java.io.Serializable

/**
 * Created by paulo on 19-10-2019.
 */
data class ProductResult(var _name: String?,
                         var _description: String?,
                         var _alias: String?,
                         var _attribute: UserProducts.Attribute?,
                         var _productResultType: ProductType?) : BaseObservable(), Serializable {

    var name: String?
        @Bindable get() = _name
        set(value) {
            _name = value
            notifyPropertyChanged(BR.name)
        }

    var description: String?
        @Bindable get() = _description
        set(value) {
            _description = value
            notifyPropertyChanged(BR.description)
        }

    var alias: String?
        @Bindable get() = _alias
        set(value) {
            _alias = value
            notifyPropertyChanged(BR.alias)
        }

    var attribute: UserProducts.Attribute?
        @Bindable get() = _attribute
        set(value) {
            _attribute = value
            notifyPropertyChanged(BR.attribute)
        }

    var productResultType: ProductType?
        @Bindable get() = _productResultType
        set(value) {
            _productResultType = value
            notifyPropertyChanged(BR.productResultType)
        }
    
    companion object {
        val EXTRA_PARAM = "ProductResult"
        val EXTRA_POSITION = "positionChanged"
        val RESULT_OK = 201
    }
}