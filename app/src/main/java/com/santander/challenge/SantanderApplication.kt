package com.santander.challenge

import android.app.Application
import com.santander.challenge.di.app.AppComponent
import com.santander.challenge.di.app.AppModule
import com.santander.challenge.di.app.DaggerAppComponent
import com.santander.challenge.utils.sharedpreferences.UserAccountPreferences

/**
 * Created by paulo on 19-10-2019.
 */
class SantanderApplication : Application() {

    companion object {
        private lateinit var mSantanderApplication: SantanderApplication
        fun getInstance(): SantanderApplication = mSantanderApplication
    }

    private var mAppComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        mSantanderApplication = this
        UserAccountPreferences.init(this)
        initInjector()
    }

    /**
     * Initialize the dagger component
     */
    private fun initInjector() {
        mAppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    fun getAppComponent() = mAppComponent
}