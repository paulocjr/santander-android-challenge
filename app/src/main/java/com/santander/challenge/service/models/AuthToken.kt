package com.santander.challenge.service.models

import com.google.gson.annotations.SerializedName

/**
 * Created by paulo on 19-10-2019.
 */
class AuthToken(@SerializedName("tokenCredential") val tokenCredential: String)