package com.santander.challenge.service.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by paulo on 19-10-2019.
 */
data class UserProducts(@SerializedName("name") val name: String,
                   @SerializedName("accounts") val accounts: List<Account>,
                   @SerializedName("cards") val cards: List<Card>) : Serializable {

    // Account product
    open class Account(@SerializedName("IBAN") val iban: String,
                       @SerializedName("alias") val alias: String,
                       @SerializedName("balance") val balance: Attribute) : Serializable

    // Card product
    open class Card(@SerializedName("pan") val pan: String,
                    @SerializedName("alias") val alias: String,
                    @SerializedName("avalaible") val available: Attribute) : Serializable


    // Attribute product
    open class Attribute(@SerializedName("value") val value: String,
                         @SerializedName("currency") val currency: String)  : Serializable
}