package com.santander.challenge.service.models

/**
 * Created by paulo on 19-10-2019.
 */
class BaseResponse<T>(val success: Boolean, val data: T?)