package com.santander.challenge.service.models

import com.google.gson.annotations.SerializedName

/**
 * Created by paulo on 19-10-2019.
 */
class User(@SerializedName("user") val user: String,
           @SerializedName("password") val password: String)