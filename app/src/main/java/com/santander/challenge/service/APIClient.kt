package com.santander.challenge.service

import android.util.Log
import com.santander.challenge.BuildConfig
import com.santander.challenge.utils.sharedpreferences.UserAccountPreferences
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by paulo on 19-10-2019.
 */
class APIClient() {

    private var mRetrofit: Retrofit

    private val tokenCredentialInterceptor = Interceptor { chain ->

        val original = chain.request()
        val builder = original.newBuilder()
            .header("Content-Type", "application/json")
            .method(original.method(), original.body())

        // Get auth token saved
        val tokenCredential = UserAccountPreferences.authToken
        tokenCredential.let{
            builder.header("tokenCredential", tokenCredential)
            Log.d("API Client", "tokenCredential Interceptor: $tokenCredential")
        }

        val request = builder.build()
        chain.proceed(request)
    }

    private fun getLoggingCapableHttpClient(): HttpLoggingInterceptor {
        val mLogging = HttpLoggingInterceptor()
        mLogging.level = HttpLoggingInterceptor.Level.BODY

        return mLogging
    }

    private fun getOkHttpClient() : OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(getLoggingCapableHttpClient())
            .addInterceptor(this.tokenCredentialInterceptor)
            .build()
    }

    init {
        mRetrofit = Retrofit
            .Builder()
            .baseUrl(BuildConfig.APP_BASE_URL)
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getRetrofit() = mRetrofit
}