package com.santander.challenge.service

import com.santander.challenge.service.models.AuthToken
import com.santander.challenge.service.models.User
import com.santander.challenge.service.models.UserProducts
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by paulo on 19-10-2019.
 */
interface UserService {

    /**
     * Request a login in backend service
     */
    @POST("login")
    fun login(@Body user: User): Call<AuthToken>

    /**
     * Retrieve all user products - Accounts and Cards
     */
    @GET("globalposition")
    fun getUserProducts(): Call<UserProducts>

}