package com.santander.challenge.viewmodel.user

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.TestObserver
import com.santander.challenge.repository.user.UserRepository
import com.santander.challenge.service.models.AuthToken
import com.santander.challenge.service.models.BaseResponse
import com.santander.challenge.service.models.UserProducts
import junit.framework.Assert
import org.junit.Before
import org.junit.Test

import org.junit.Rule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by pcamilo on 21/10/2019
 */

@RunWith(JUnit4::class)
class UserViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var userRepository: UserRepository

    lateinit var userViewModel: UserViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        this.userViewModel = UserViewModel(this.userRepository)
    }

    @Test
    fun loginSuccess() {
        // Given
        val data = MutableLiveData<BaseResponse<AuthToken>>()
        data.value = BaseResponse(true, AuthToken("asaujbjx87ax7xbzzz="))

        Mockito.`when`(userRepository.login("12344F", "1234")).thenReturn(data)
        val value = TestObserver.test(userViewModel.login("12344F", "1234")).value()

        // Then
        Assert.assertTrue(value.success)
    }

    @Test
    fun loginFailed() {
        // Given
        val data = MutableLiveData<BaseResponse<AuthToken>>()
        data.value = BaseResponse(false, AuthToken(""))

        Mockito.`when`(userRepository.login("", "")).thenReturn(data)
        val value = TestObserver.test(userViewModel.login("", "")).value()

        // Then
        Assert.assertTrue(!value.success)
    }

    @Test
    fun userProductsSuccess() {
        // Given
        val data = MutableLiveData<BaseResponse<UserProducts>>()
        data.value = BaseResponse(true, UserProducts("userTest", arrayListOf(), arrayListOf()))

        Mockito.`when`(userRepository.getUserProducts()).thenReturn(data)
        val value = TestObserver.test(userViewModel.userProducts()).value()

        // Then
        Assert.assertTrue(value.success)
    }

    @Test
    fun userProductsFailed() {
       // Given
        val data = MutableLiveData<BaseResponse<UserProducts>>()
        data.value = BaseResponse(false, UserProducts("userTest", arrayListOf(), arrayListOf()))

        Mockito.`when`(userRepository.getUserProducts()).thenReturn(data)
        val value = TestObserver.test(userViewModel.userProducts()).value()

        // Then
        Assert.assertTrue(!value.success)
    }
}