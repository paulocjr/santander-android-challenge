package com.santander.challenge

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import com.santander.challenge.view.login.LoginActivity
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
@LargeTest
class LoginActivityInstrumentedTest {

    @Rule
    @JvmField
    val rule = IntentsTestRule(LoginActivity::class.java)

    @Test
    fun validateFieldPassword() {
        onView(withId(R.id.edtPassword)).check(matches(withHint(R.string.hint_edt_password)))
        onView(withId(R.id.edtPassword)).check(matches(withText("")))
    }

    @Test
    fun validateLoginButton() {
        onView(withId(R.id.btnLogin)).check(matches(withText(R.string.btn_login)))
    }

    @Test
    fun validateButtonEnabled() {
        onView(withId(R.id.edtUser)).perform(typeText("12345G"),closeSoftKeyboard())
        onView(withId(R.id.edtPassword)).perform(typeText("1234"),closeSoftKeyboard())
        onView(withId(R.id.btnLogin)).check(matches(isEnabled()))
    }

    @Test
    fun validateButtonDisabled() {
        onView(withId(R.id.edtUser)).perform(typeText(""),closeSoftKeyboard())
        onView(withId(R.id.edtPassword)).perform(typeText(""),closeSoftKeyboard())
        onView(withId(R.id.btnLogin)).check(matches(not(isEnabled())))
    }
}
